<?php

namespace Drupal\webform_file_upload_to_salesforce\EventSubscriber;

use Drupal\file\Entity\File;
use Drupal\salesforce\Event\SalesforceEvents;
use Drupal\salesforce_mapping\Event\SalesforcePushParamsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\salesforce_mapping\Entity\MappedObject;

/**
 * Attach files from webform to their object.
 *
 * After webforms are posted to Salesforce, check the form for files, and
 * attach the files to whatever object was just created. This will most likely
 * be a Case, but this listener should adapt itself to work for any SObject
 * which supports Attachments.
 */
class WebformFileUploadToSalesforceSubscriber implements EventSubscriberInterface {

    /**
     * Core file system interface.
     *
     * @var \Drupal\Core\File\FileSystemInterface
     */
    protected $fileSystem;

    /**
     * Salesforce Suite Rest Client.
     *
     * @var \Drupal\salesforce\Rest\RestClient
     */
    protected $sfClient;

    /**
     * The Messenger service.
     *
     * @var \Drupal\Core\Messenger\MessengerInterface
     */
    protected $messenger;

    /**
     * SalesforcePushParamsEvent push success callback.
     *
     * @param \Drupal\salesforce_mapping\Event\SalesforcePushParamsEvent $event
     *   The event.
     */
    public function attachMappedFile(SalesforcePushParamsEvent $event) {
        $mappedObject = $event->getMappedObject();
        $entity = $event->getEntity();
        if ($entity->getEntityType()->id() == 'webform_submission') {
            $webform = $entity->getWebform();
            $fields = $webform->getElementsOriginalDecoded();
            $data = $entity->getData();
            $leadFlag = false;
            $campaignFlag = false;
            foreach ($fields as $fld => $fValue) {
                if(isset($fValue['#custom_lead']) && ($leadFlag == false)){
                    $leadFlag = true;
                    $file = File::load($data[$fld]);
                    $path = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
                    $fileName = $file->getFilename();
                    $this->handleFile($path, $mappedObject, $fileName);
                }
                if(isset($fValue['#custom_campaign_id']) && ($campaignFlag == false)){
                    $campaignFlag = true;
                    $cid = $entity->getElementData($fld);
                    $this->handleCampaign($mappedObject, $cid);
                }
            }
        }
    }

    /**
     * Handle the upload of just one file.
     *
     * @param int $fid
     *   Drupal file ID.
     * @param \Drupal\salesforce_mapping\Entity\MappedObject $mappedObject
     *   Mapped object for the parent form submission.
     */
    private function handleFile($filePath, MappedObject $mappedObject, $fileName) {
        $handle = fopen($filePath, 'rb');
        $content = base64_encode(fread($handle, filesize($filePath)));
        $parameters = [
            'ParentId' => $mappedObject->sfid(),
            'Name' => $fileName,
            'body' => $content,
        ];
        $response = \Drupal::service('salesforce.client')->objectCreate('Attachment', $parameters);
        if (!$response) {
            $this->messenger->addError('We were unable to attach your file to the request. We may be in touch to get it another way.');
        }
    }

    /**
     * Handle the campaign connection.
     *
     * @param \Drupal\salesforce_mapping\Entity\MappedObject $mappedObject
     *   Mapped object for the parent form submission.
     */
    private function handleCampaign(MappedObject $mappedObject, $CampaignId) {
        //Code to load the campaign ID from the webform submission.
        $data = [
            'CampaignId' => $CampaignId,
            'LeadId' => $mappedObject->sfid(),
            'Status' => 'Sent',
        ];
        $campaignResponse = \Drupal::service('salesforce.client')->objectCreate('CampaignMember', $data);
        if (!$campaignResponse) {
            $this->messenger->addError('We were unable to attach campaign with this lead.');
        }
    }


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      SalesforceEvents::PUSH_SUCCESS => 'attachMappedFile',
    ];
    return $events;
  }

}
