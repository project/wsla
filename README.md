Module Configuratoin URL: /admin/config/webform_file_upload_to_salesforce/settings
You need to provide the login credentials in addition to the salesforce URL which is for REST connectivity for uploading a file
You can get this on clicking profile icon after application name you will get the url.
https://SALESFORCE_URL/services/data/vSALESFOCE_VERSION/sobjects/Attachment
For e.g.: https://mysalesforce.my.salesforce.com/services/data/v46.0/sobjects/Attachment
